using System;
using Microsoft.AspNetCore.Mvc;

namespace TC.Timesheet.Customer.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CustomersController : ControllerBase
    {
        [HttpGet]
        public object Get()
        {
            return new[] {new {Id = Guid.NewGuid()}};
        }
    }
}