using Microsoft.AspNetCore.Mvc;

namespace TC.Timesheet.Customer.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class HealthController : ControllerBase
    {
        [HttpHead]
        public IActionResult Head()
        {
            return Ok();
        }
    }
}