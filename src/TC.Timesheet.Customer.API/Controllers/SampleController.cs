﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace TC.Timesheet.Customer.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class SampleController : ControllerBase
    {
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var random = new Random();

            await Task.Delay(TimeSpan.FromSeconds(random.Next(1, 4)));
            
            return Ok(new {});
        }
    }
}