using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using TC.Timesheet.Customer.Data.EF;

namespace TC.Timesheet.Customer.API.HealthChecks
{
    public class DBVersionHealthCheck : IHealthCheck
    {
        private readonly IServiceProvider _services;
        private static bool _initialized, _executingMigrations;

        public DBVersionHealthCheck(IServiceProvider services)
        {
            _services = services;
        }

        public Task<HealthCheckResult> CheckHealthAsync(
            HealthCheckContext context,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            try
            {
                var entities = _services.GetService<TimesheetEntities>();

                if (!_initialized && !_executingMigrations)
                {
                    _executingMigrations = true;
                    entities.Database.Migrate();
                    _initialized = true;
                }

                var _ = entities.Database.ExecuteSqlRaw("select 1");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToStringDemystified());

                return Task.FromResult(HealthCheckResult.Unhealthy("An unhealthy result."));
            }

            return Task.FromResult(HealthCheckResult.Healthy("DB Ready and Updated"));
        }
    }
}