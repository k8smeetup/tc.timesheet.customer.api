using App.Metrics;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using TC.Timesheet.Customer.API.HealthChecks;
using TC.Timesheet.Customer.Data.EF;

namespace TC.Timesheet.Customer.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<TimesheetEntities>();

            services.AddControllers();

            services.AddMetricsEndpoints();
            services.AddMetricsReportingHostedService();
            
            var metrics = AppMetrics.CreateDefaultBuilder()
                .Build();

            services.AddMetrics(metrics);
            services.AddMetricsTrackingMiddleware();

            services.AddHealthChecks().AddCheck<DBVersionHealthCheck>("Database version");
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapHealthChecks("/health");
            });
            
            app.UseMetricsActiveRequestMiddleware();
            app.UseMetricsApdexTrackingMiddleware();
            app.UseMetricsErrorTrackingMiddleware();
            app.UseMetricsRequestTrackingMiddleware();
            app.UseMetricsPostAndPutSizeTrackingMiddleware();
        }
    }
}