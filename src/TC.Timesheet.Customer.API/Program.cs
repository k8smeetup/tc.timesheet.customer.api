using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using App.Metrics;
using App.Metrics.AspNetCore;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace TC.Timesheet.Customer.API
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
#if RELEASE
        .ConfigureMetricsWithDefaults(c =>
                {
                    c.Report.ToInfluxDb("http://influxdb:8086", "appmetrics");
                })
                .UseMetricsEndpoints()
                .UseMetricsWebTracking()
#endif

                //.ConfigureWebHost(SetSecureChannel)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                    
                });

        private static void SetSecureChannel(IWebHostBuilder webHostBuilder)
        {
            webHostBuilder.UseKestrel(options =>
            {
                options.Listen(IPAddress.Any, 443, listenOptions =>
                {
                    //listenOptions.UseHttps("cert.pfx");
                });
            });
        }
    }
}