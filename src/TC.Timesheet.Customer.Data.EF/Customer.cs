using System;
using System.ComponentModel.DataAnnotations;

namespace TC.Timesheet.Customer.Data.EF
{
    public class Customer
    {
        public Customer()
        {
            Id = Guid.NewGuid();
        }

        [Key]
        public Guid Id { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        [Required]
        public int AmountPerHour { get; set; }

        public bool Deleted { get; set; }
    }
}