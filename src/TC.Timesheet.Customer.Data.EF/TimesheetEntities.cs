using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace TC.Timesheet.Customer.Data.EF
{
    public class TimesheetEntities : DbContext
    {
        public static readonly ILoggerFactory ConsoleLogger = LoggerFactory.Create(builder =>
        {
            builder.AddConsole();
        });

        public DbSet<Customer> Customers { get; set; }
        //public DbSet<TimesheetItem> TimesheetItems { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
#if RELEASE
            var connectionString =
                "Server=timesheet-customer-database;Database=TC.Customer;User Id=sa;Password=Use4SqlServer2020*01;Trusted_Connection=True;Integrated Security=No;MultipleActiveResultSets=true;";
            optionsBuilder.UseSqlServer(connectionString);
#else
            optionsBuilder.UseLoggerFactory(ConsoleLogger);
            var connectionString =
                "Server=.;Database=TC.Customer;User Id=sa;Password=Use4SqlServer2020*01;Trusted_Connection=True;Integrated Security=No;MultipleActiveResultSets=true;";
            optionsBuilder.UseSqlServer(connectionString);
#endif
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Customer>(c => c.HasQueryFilter(e => !e.Deleted));

            base.OnModelCreating(modelBuilder);
        }
    }
}